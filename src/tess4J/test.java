package tess4J;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;

import java.awt.Color;
import java.awt.FlowLayout;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.JOptionPane;

import java.awt.event.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Formatter;

import net.sourceforge.tess4j.*;
@SuppressWarnings("serial")
public class test extends JFrame implements ActionListener{

	final Formatter x;
	private ImageIcon Icon;
	private JFileChooser filechooser;
	private JButton openbutton;
	int returnvalue;
	String currentline;
	
	
	public test() throws FileNotFoundException {
		super("Sadman's OCR");
		setLayout(new FlowLayout());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(500,500);
		getContentPane().setBackground(Color.ORANGE);
		initComponents();
		filechooser = new JFileChooser();
		filechooser.setCurrentDirectory(new File("images"));
		filechooser.setFileFilter(new FileNameExtensionFilter("PNG","png","bmp","uzn","tif"));
		openbutton = new JButton("select Picture");
		add(openbutton);
		openbutton.setBounds(84, 145, 100, 25);
		openbutton.addActionListener(this);
		x = new Formatter("a.txt");
		
	}
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == openbutton) {
			returnvalue = filechooser.showOpenDialog(this);
			if(returnvalue == JFileChooser.APPROVE_OPTION) {
				try {
					ITesseract instance = new Tesseract();
					instance.setDatapath("C:\\Users\\Sadman\\eclipse-workspace\\OCR\\tessdata");
					String outputs = instance.doOCR(filechooser.getSelectedFile());
					JOptionPane.showMessageDialog(null, outputs, "OCR", JOptionPane.PLAIN_MESSAGE);
					x.format("%s", outputs);
					x.close();
					
				}
				catch(TesseractException ioe) {
					System.err.println(ioe.getMessage());
					System.out.println("an error has occured");
				}
			}
		}
		
	}
	
	public void initComponents()
	{
		
		Icon = new ImageIcon(getClass().getResource("ocrICON.png"));
		this.setIconImage(Icon.getImage());
	}
	public static void main(String[] args) throws FileNotFoundException  {
		
		test add = new test();
		
		add.setVisible(true);
		
	}

}
